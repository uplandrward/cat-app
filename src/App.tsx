import React from 'react';
import MainAppBar from "./components/layout/MainAppBar";
import CatCard from "./components/cards/CatCard";

function App() {
    return (
        <div className="App">
            <MainAppBar/>
            <CatCard/>
        </div>
    );
}

export default App;
