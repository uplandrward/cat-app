import {Box, IconButton} from "@mui/material";
import SkipPreviousIcon from "@mui/icons-material/SkipPrevious";
import SkipNextIcon from "@mui/icons-material/SkipNext";

export default function PreviousNextButtons(props: { previousOnClick: () => void, nextOnClick: () => void }) {
    return <Box sx={{display: "flex", alignItems: "center", pl: 1, pb: 1}}>
        <IconButton aria-label="previous" onClick={props.previousOnClick}>
            <SkipPreviousIcon/>
        </IconButton>
        <IconButton aria-label="next" onClick={props.nextOnClick}>
            <SkipNextIcon/>
        </IconButton>
    </Box>;
}
