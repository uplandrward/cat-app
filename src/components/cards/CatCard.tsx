import {Box, Card, CardMedia, useTheme} from "@mui/material";
import {useEffect, useState} from "react";
import catKey from "../../catKey";
import PreviousNextButtons from "./PreviousNextButtons";
import CatCardContent from "./CatCardContent";

export interface CatImage {
    url: string;
    original_filename: string;
}

export default function CatCard() {
    const [cats, setCats] = useState<CatImage[]>([]);
    const [catIndex, setCatIndex] = useState<number | null>(null);
    useEffect(
        () => {
            fetch(
                'https://api.thecatapi.com/v1/images?limit=100',
                {
                    headers: {'x-api-key': catKey}
                }
            ).then(response => response.json().then(json => setCats(json)));
        },
        []
    );

    if (cats.length > 0 && catIndex === null) {
        setCatIndex(0);
    }

    const theme = useTheme();

    return (
        <Card sx={{display: 'flex'}}>
            <Box sx={{display: 'flex', flexDirection: 'column'}}>
                <CatCardContent catIndex={catIndex} catImages={cats}/>
                <PreviousNextButtons previousOnClick={() => {
                    if (catIndex !== null && catIndex > 0) {
                        setCatIndex(catIndex - 1);
                    }
                }} nextOnClick={() => {
                    if (catIndex !== null && catIndex < cats.length - 1) {
                        setCatIndex(catIndex + 1);
                    }
                }}/>
            </Box>
            {
                catIndex !== null && (
                    <CardMedia
                        component="img"
                        sx={{width: 151}}
                        image={cats[catIndex].url}
                        alt="Picture of a cat"
                    />
                )
            }

        </Card>
    );
}
