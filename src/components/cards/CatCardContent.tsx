import {CardContent, Typography} from "@mui/material";
import {CatImage} from "./CatCard";

export default function CatCardContent(props: { catIndex: number | null, catImages: CatImage[] }) {
    return <CardContent sx={{flex: "1 0 auto"}}>
        <Typography component="div" variant="h5">
            Select your cat
        </Typography>
        <Typography variant="subtitle1" color="text.secondary" component="div">
            {props.catIndex !== null && props.catImages[props.catIndex].original_filename}
        </Typography>
    </CardContent>;
}
