import {AppBar, IconButton, Toolbar, Tooltip, Typography} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import UploadIcon from "@mui/icons-material/Upload";
import React from "react";
import catKey from "../../catKey";

export default function MainAppBar() {
    return (
        <AppBar position="static">
            <Toolbar variant="dense">
                <IconButton edge="start" color="inherit" aria-label="menu" sx={{mr: 2}}>
                    <MenuIcon/>
                </IconButton>
                <Typography variant="h6" color="inherit" component="div" sx={{flexGrow: 1}}>
                    Cat App
                </Typography>
                <input accept="image/*" id="catUploadInput" type="file" hidden onChange={event => {
                    if (event.target && event.target.files) {
                        const formData = new FormData();
                        formData.append('file', event.target.files[0]);
                        fetch(
                            'https://api.thecatapi.com/v1/images/upload',
                            {
                                method: 'post',
                                body: formData,
                                headers: {'x-api-key': catKey}
                            }
                        );
                    }
                }}/>
                <label htmlFor="catUploadInput">
                    <IconButton component="span" color="inherit" sx={{mr: 2}}>
                        <Tooltip title="Upload">
                            <UploadIcon/>
                        </Tooltip>
                    </IconButton>
                </label>
            </Toolbar>
        </AppBar>
    );
}